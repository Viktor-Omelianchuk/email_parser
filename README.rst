============
email_parser
============

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
     :target: https://github.com/ambv/black
     :alt: Black code style

:License: MIT

Script for searching headers, IPs, domains in a file with email content.

.. contents::

Installation
-------------------
On Unix, Linux, BSD, macOS, and Cygwin::

  $ git clone git://gitlab.com/Viktor-Omelianchuk/email_parser

Create and activate isolated Python environments
-------------------------------------------------
::

    $ cd email_parser
    $ virtualenv env
    $ source env/bin/activate

Install requirements
--------------------------------------
::

    $ pip install -r requiremtns.txt


Arguments and Usage
--------------------------------------
Usage
=====

::

    usage: email_parser [-h] [-f --file]
                        [-ll --logging-level] [-hp --header-pattern]
                        [-hs --header-string]
                        [-sdb --save-to-db]


Arguments
=========
Quick reference table
=========================
+---------+----------------------+-------------------------+-------------------------------------+
|Short    |Long                  |Default                  |Description                          |
+---------+----------------------+-------------------------+-------------------------------------+
| ``-h``  |``--help``            |                         |Show help                            |
+---------+----------------------+-------------------------+-------------------------------------+
| ``-f``  |``--file``            | ``''``                  |Path to file with email content      |
+---------+----------------------+-------------------------+-------------------------------------+
|``-ll``  |``--logging-level``   |``INFO``                 |Level for logging module             |
+---------+----------------------+-------------------------+-------------------------------------+
|``-hp``  |``--header-pattern``  |``''``                   |Regex header pattern                 |
+---------+----------------------+-------------------------+-------------------------------------+
|``-hs``  |``--header-string``   |``''``                   |String header pattern                |
+---------+----------------------+-------------------------+-------------------------------------+
|``-sdb`` |``--save-to-db``      |``False``                |Save ip and domain to database       |
+---------+----------------------+-------------------------+-------------------------------------+

Examples to use
--------------------------------------
Get headers by string or regex pattern or both
==================================================
::

    $ python email_parser.py -f file_with_email_content -hs Received
    or
    $ python email_parser.py -f file_with_email_content -hp '^Received:*'
    or
    $ python email_parser.py -f file_with_email_content -hs Received -hp '^ARC-Seal:*'

The script will find all headers that match the string or regular expression pattern and output the result to the terminal

Other argumens
====================
::

    $ python email_parser.py -f file_with_email_content -hs Received -sdb True
    $ python email_parser.py -f file_with_email_content -hs Received -ll DEBAG

By default ip addresses and domains name will be output to terminal
For save ip and domain name after parse email file to database use -sdb and value True
For changing logging level use argument -ll and value from the next list: [DEBUG, INFO, WARNING, ERROR, CRITICAL]

Start local MySQL server
---------------------------

  $ docker-compose up -d