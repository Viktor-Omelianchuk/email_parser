"""Main module."""
import argparse
import email
import json
import logging
import logging.config
import os
import re
import sys
from collections import Counter

import mysql.connector


def parse_arguments():
    """
    The function allows you to get command line data

    :return (argparse.Namespace): Return command line arguments and their value
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f",
        "--file",
        type=str,
        default="",
        required=True,
        help="Path to file with email content",
    )
    parser.add_argument(
        "-ll",
        "--logging-level",
        type=str,
        default="INFO",
        help="level for logging module",
    )
    parser.add_argument(
        "-hp",
        "--header-pattern",
        type=str,
        default="",
        help="Regex header pattern for searching into file with email content",
    )
    parser.add_argument(
        "-hs",
        "--header-string",
        type=str,
        default="",
        help="Header string for searching into file with email content",
    )
    parser.add_argument(
        "-sdb",
        "--save-to-db",
        type=bool,
        default=False,
        help="Save IP addresses and domains to database",
    )

    console_arguments = parser.parse_args()

    if not os.path.exists(console_arguments.file):
        raise FileExistsError

    return console_arguments


def search_headers_by_string(header_string: str, email_message) -> list:
    """
    The function searches and returns the headers and headers body
    corresponding the header_string in the file with the email content

    :param header_string: (str), string with header. Example : 'Received'
    :param email_message: (email.message.Message), Object with email content
    :return (list), Returns a list of strings containing the header
    and header value
    """
    log = logging.getLogger("main.search_headers_by_string")

    result = []

    for header_name, value in email_message.items():
        if header_name.lower() == header_string.lower():
            result.append(f"{header_name}: {value}")
            log.debug(
                "Header %s was added to result with value %s"
                % (header_name, value)
            )
    return result


def search_by_regex(header_pattern: str, email_message) -> list:
    """
    Function search headers in the file with the content of the letter and
    returns the headers and body of headers matching
    the regular expression header_pattern

    :param header_pattern: (str), Regex pattern. Example : '^Received:*'
    :param email_message: (email.message.Message), Object with email content
    :return (list), Returns a list of strings containing the header
    and header value
    """
    log = logging.getLogger("main.search_by_regex")

    result = []
    header_regex = re.compile(header_pattern)

    for header_name, value in email_message.items():
        full_header = f"{header_name}: {value}"
        if header_regex.match(full_header):
            result.append(full_header)
            log.debug(
                "Header %s was added to result with value %s"
                % (header_name, value)
            )
    return result


def get_list_of_ip(readed_email_file: str) -> list:
    """
    The function searches for IPv4 addresses in the string with email content.
    Returns the list of found IP addresses

    :param readed_email_file: (str), String with email content
    :return (list), Returns a list of ip addresses
    """
    log = logging.getLogger("main.get_list_of_ip")

    ip_pattern = (
        r"(?:^|\b(?<!\.))(?:1?\d?\d|2[0-4]\d|25[0-5])"
        r"(?:\.(?:1?\d?\d|2[0-4]\d|25[0-5])){3}(?=$|[^\w.])"
    )

    list_of_found_ip = re.findall(ip_pattern, readed_email_file)

    log.debug("a list of IP addresses found: %s" % list_of_found_ip)

    return list_of_found_ip


def get_list_of_domain(readed_email_file: str) -> list:
    """
    The function searches for domains in the string with email content.
    Returns the list of found domains

    :param readed_email_file: (str), String with email content
    :return (list), Returns a list of found domains
    """
    log = logging.getLogger("main.get_list_of_domain")

    domain = re.findall(r"(?:[a-zA-Z0-9]+\.)+[a-z]{2,10}", readed_email_file)

    log.debug("a list of domain found: %s" % domain)

    return domain


def save_data_to_db(
    con: mysql.connector.connection.MySQLConnection,
    table_name: str,
    list_of_data: list,
):
    """
    The function saves ip or domain to database

    :param con: (mysql.connector.connection.MySQLConnection),
    Connection to database
    :param table_name: (str), Name of table and column
    :param list_of_data: (list), List of data to save
    """
    log = logging.getLogger("main.save_data_to_db")

    try:
        sql_cursor = con.cursor()

        create_table_sql = f"""CREATE TABLE IF NOT EXISTS {table_name} (
                   id int NOT NULL AUTO_INCREMENT,
                   {table_name} varchar(45) NOT NULL,
                   PRIMARY KEY (id))"""
        sql_cursor.execute(create_table_sql)
        con.commit()

        list_of_data = [(element,) for element in list_of_data]
        insert_sql = f"""INSERT INTO {table_name} ({table_name}) VALUES (%s)"""
        sql_cursor.executemany(
            insert_sql,
            list_of_data,
        )
        con.commit()
        log.debug(
            "Information from list %s was added to database" % list_of_data
        )
    except mysql.connector.errors as error:
        log.error("%s Error while working with SQLite" % error)


def data_output_to_terminal(
    list_of_title: list, list_with_data: list
):
    """
    The function displays data in the form of a table
    :param list_of_title: (list), List of strings that will become the title
    :param list_with_data: (list), List of rows that will fill the table
    :return:
    """
    log = logging.getLogger('main.data_output_to_terminal')
    data = [list_of_title] + [list(x) for x in list_with_data]

    result_string = '\n\n'
    if list_with_data:
        max_len = len(sorted(data, key=lambda x: x[0])[0][0]) + 10
        for index, element in enumerate(data):
            line = "".join(str(x).ljust(max_len) for x in element) + '\n'
            result_string += line
            if index == 0 or index == len(data) - 1:
                result_string += '\n'
        result_string += "-" * 80
        log.info(result_string)


def main():
    """
    The function performs actions depending on command line arguments:
    Search for matching headers, save to database or output to terminal
    """

    console_arguments = parse_arguments()

    path_to_file = console_arguments.file
    header_pattern = console_arguments.header_pattern
    header_string = console_arguments.header_string

    if os.path.exists("logging.json"):
        with open("logging.json", "rt") as f:
            config = json.load(f)
            config["loggers"]["main"][
                "level"
            ] = console_arguments.logging_level
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=logging.INFO)

    logger = logging.getLogger("main")

    result_header_list = []

    with open(path_to_file, "r") as file:
        email_message = email.message_from_file(file)
        readed_email_file = email_message.as_string()

    if header_pattern:
        for header in search_by_regex(header_pattern, email_message):
            if header not in result_header_list:
                result_header_list.append(header)
    if header_string:
        for header in search_headers_by_string(header_string, email_message):
            if header not in result_header_list:
                result_header_list.append(header)

    for full_header in result_header_list:
        logger.info(full_header)

    list_of_ip = get_list_of_ip(readed_email_file)
    list_of_domain = get_list_of_domain(readed_email_file)

    if console_arguments.save_to_db:
        con = mysql.connector.connect(
            user="user",
            password="password",
            host="127.0.0.1",
            port=3308,
            database="email",
        )
        save_data_to_db(con, "ip", list_of_ip)
        save_data_to_db(con, "domain", list_of_domain)
        con.close()
    else:
        ip_counter = Counter(list_of_ip).most_common()
        domains_counter = Counter(list_of_domain).most_common()
        ip_table_headers = ["IP", "Count"]
        domain_table_headers = ["Domain", "Count"]
        data_output_to_terminal(ip_table_headers, ip_counter)
        data_output_to_terminal(domain_table_headers, domains_counter)


if __name__ == "__main__":
    sys.exit(main())
